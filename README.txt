Configure Qt
============

This package provides a script that runs to present configuration options for
the Qt cross-platform framework in a readable manner, using a local web
server to accept input from the user via a browser.

To use the script, enter the directory in which you plan to build Qt and run
the configure-qt.py script, passing the path to Qt's configure script as an
argument in the following way:

  /path/to/configure-qt.py /path/to/qt/configure

The script should start a web server on port 8000. Open a web browser and
navigate to http://localhost:8000 to customise the Qt configuration. Change
any options that need customisation and update the configuration as necessary
by pressing the "Update Configuration" button at the bottom of the page.

When you are ready to accept the configuration, scroll to the bottom of the
page and click "Accept Configuration". A page will show the command that you
should run to configure Qt with your selected options. This command is also
written to the terminal where you ran the configure-qt.py script.

Known Issues
------------

The -xvideo command line switch appears in the options presented by Qt's
configure script but is often rejected by the script itself. You probably
want to select -no-xvideo to avoid problems unless you are absolutely sure
you need it.

-- 
David Boddie <david@boddie.org.uk>
