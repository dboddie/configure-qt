#!/usr/bin/env python

"""
configure-qt.py

Copyright (C) 2009 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import cgi, os, popen2, SimpleHTTPServer, select, sys, threading

__version__ = "0.2"


reconfigure_header_template = \
"""<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=%(charset)s">
  <title>%(title)s</title>
  <style type="text/css">
    .group {
        background-color: #ffdd99;
        border: solid black 1pt;
        margin: 0.5em;
        padding: 0.5em
    }
    
    h1 {
        text-align: center
    }
    
    .output {
        background-color: #eeeeee;
        border: dotted black 1px;
        padding: 0.5em
    }
  </style>
</head>

<body>
<h1>%(title)s</h1>

<form action="/" enctype="%(charset)s">
"""

reconfigure_footer_template = \
"""<p>
  <input type="submit" name="action" value="Update Configuration">
  <input type="submit" name="action" value="Accept Configuration">
</p>
</form>
</body>
</html>
"""

processing_template = \
"""<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=%(charset)s">
  <title>%(title)s</title>
  <style type="text/css">
    h1 {
        text-align: center
    }
    
    .output {
        background-color: #eeeeee;
        border: dotted black 1px;
        padding: 0.5em
    }
  </style>
</head>

<body>
<h1>%(title)s</h1>

<p>
  The command to use has been written to the terminal you used to start
  the web server. It is included here for future reference:
</p>

%(command)s

</body>
</html>
"""

class ConfigurationError(Exception):

    pass


class Section:

    def __init__(self, header, description, groups):
    
        self.header = header
        self.description = description
        self.groups = groups


class Group:

    def __init__(self, description, options, identifier):
    
        self.description = description
        self.options = options
        self.identifier = identifier


class Option:

    def __init__(self, name, argument, description, default, evaluate, group = ""):
    
        self.name = name
        self.argument = argument
        self.description = description
        self.default = default
        self.evaluate = evaluate


class ConfigurationParser:

    option_markers = ("*", "+", "-")
    
    def __init__(self, text):
    
        self.lines = text.splitlines()
    
    def parse(self):
    
        index = self.find_start()
        if index is None:
            raise ConfigurationError, "Failed to parse configure script output."
        
        return self.find_sections(index)
    
    def find_start(self):
    
        index = 0
        for line in self.lines:
        
            if line.startswith("Installation options:"):
                return index
            
            index += 1
        
        return None
    
    def find_sections(self, index):
    
        sections = []
        
        while index < len(self.lines):
        
            line = self.lines[index]
            
            text = line.lstrip()
            indentation = len(line) - len(text)
            
            if indentation == 0 and text:
            
                section, index = self.read_section(text, index)
                sections.append(section)
            
            else:
                index += 1
        
        return sections
    
    def read_section(self, header, index):
    
        description = []
        
        index += 1
        
        while index < len(self.lines):
        
            line = self.lines[index].strip()
            if line:
                break
            
            index += 1
        
        section = Section(header, "", [])
        options = []
        
        while index < len(self.lines):
        
            line = self.lines[index]
            
            text = line.lstrip()
            indentation = len(line) - len(text)
            
            if indentation == 0:
            
                if options:
                    section.groups += self.create_groups(
                        header, len(section.groups), " ".join(description),
                        options
                        )
                    options = []
                
                elif description and not section.groups:
                    section.description = " ".join(description)
                
                description = []
                
                if text:
                    break
            
            if text[:1] in self.option_markers:
                option, index = self.read_option(index, text)
                options.append(option)
            
            else:
                text = text.strip()
                if text:
                    description.append(text)
                index += 1
        
        return section, index
    
    def read_option(self, index, text):
    
        default = False
        evaluate = False
        
        if text.startswith("*"):
            default = True
            text = text[1:].lstrip()
        
        elif text.startswith("+"):
            default = True
            evaluate = True
            text = text[1:].lstrip()
        
        if text[:1] != "-":
            raise ConfigurationError, "Option expected but not found on line %i." % (index + 1)
        
        space = text.find(" ", 1)
        dots = text.find(".", 1)
        comma = text.find(",", 1)
        
        if dots != -1 and comma != -1:
            ### Hack for -help and -verbose options.
            dots = min(dots, comma)
        elif dots == -1:
            dots = comma
        
        if dots == -1 and space == -1:
            raise ConfigurationError, "Cannot read option name on line %i." % (index + 1)
        elif dots != -1 and space != -1:
            ### Hack for -xmlpatterns... option.
            space = min(space, dots)
        elif space == -1:
            space = dots
        
        name = text[:space]
        
        text = text[space:]
        
        argument = ""
        
        i = 0
        while i < len(text):
            if text[i] == "<":
                argument += "<"
            elif text[i] == ">":
                argument += ">"
                i += 1
                break
            elif text[i] == " ":
                pass
            elif argument != "":
                argument += text[i]
            else:
                break
            i += 1
        
        # Strip leading ...
        j = i
        
        while j < len(text):
            if text[j] == " ":
                j += 1
            elif text[j] == ".":
                j += 1
            else:
                break
        
        ### Hack for -platform target ... option.
        dots = text.find("...", j)
        if j == i and not argument and dots > j:
        
            ### Hack for -help and -verbose options.
            if text[j] == ",":
                j = dots
            else:
                # It looks like we may have missed an argument that wasn't marked up.
                argument = text[:dots].strip()
            
            j = dots + 3
            
            while j < len(text):
                if text[j] == " ":
                    j += 1
                elif text[j] == ".":
                    j += 1
                else:
                    break
        
        description = [text[j:].strip()]
        index += 1
        
        while index < len(self.lines):
        
            line = self.lines[index]
            
            text = line.lstrip()
            indentation = len(line) - len(text)
            
            if indentation == 0:
                break
            
            if text[:1] in self.option_markers:
                break
            
            description.append(text.strip())
            index += 1
        
        return Option(name, argument, " ".join(description), default, evaluate), index
    
    def create_groups(self, header, index, description, options):
    
        option_dict = {}
        breaks = []
        
        # Find where the options should be split into separate groups by
        # looking for the first option in a group that has a -no- option.
        # This may be the -no- option itself.
        
        for option in options:
        
            if option.name.startswith("-no-"):
                name = option.name[3:]
                other = name
            else:
                name = option.name
                other = "-no" + option.name
            
            if not option_dict.has_key(name):
                option_dict[name] = option
            else:
                breaks.append(other)
        
        groups = []
        group_options = []
        group_index = 0
        
        for option in options:
        
            if option.name in breaks:
            
                # Create a group containing any previous options.
                if group_options:
                
                    identifier = "".join(map(str, (header, index + group_index)))
                    groups.append(Group(description, group_options, identifier))
                    group_index += 1
                    group_options = []
            
            group_options.append(option)
        
        if group_options:
        
            identifier = "".join(map(str, (header, index + group_index)))
            groups.append(Group(description, group_options, identifier))
        
        return groups


class ConfigurationRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    server_version = "configure-qt/" + __version__
    
    def handle(self):
    
        SimpleHTTPServer.SimpleHTTPRequestHandler.handle(self)
    
    def do_GET(self):
        """Serve a GET request."""
        
        # Simple access control
        if self.client_address[0] != "127.0.0.1":
            content_type, data = None, None
        else:
            # The server holds the states of all the clients, so ask the server
            # to return the information. The returned value is a tuple containing
            # the content type and the data.
            if self.path.startswith("/"):
                query = self.path.find("?")
                if query != -1:
                    options = cgi.parse_qs(self.path[query + 1:])
                else:
                    options = {}
                
                content_type, data = self.perform_request(self.path, options)
            else:
                content_type, data = None, None
        
        # Ensure that the headers are set correctly for this page and send
        # the data to the client.
        self.send_head(content_type, data)
    
    def do_HEAD(self):
        """Serve a HEAD request."""
        
        # Translate the path passed by the client into a request for
        # information.
        if self.path == "/":
            request = "show options"
        else:
            request = None
        
        # The server holds the states of all the clients, so ask the server
        # to return the information. The returned value is a tuple containing
        # the content type and the data.
        content_type, data = self.perform_request(request)
        
        # Ensure that the headers are set correctly for this page.
        self.send_head(content_type)
    
    def tag(self, name, contents = ""):
    
        if contents:
            return "<"+name+">"+self.html(contents)+"</"+name+">"
        else:
            return "<"+name+" />"
    
    # Modified version of send_head from SimpleHTTPRequestHandler
    
    def perform_request(self, request, form_options = {}):
    
        if form_options.get("action") == ["Accept Configuration"]:
            return self.configure_qt(request, form_options)
        elif form_options.get("action") == ["Update Configuration"]:
            return self.reconfigure_qt(request, form_options)
        elif form_options.get("action") == ["View Results"]:
            return self.view_results(request, form_options)
        else:
            return self.reconfigure_qt(request, form_options)
    
    def process_option(self, group, option, form_options):
    
        options_used = []
        body = []
        
        start = option.name.find("<")
        end = option.name.rfind(">")
        if -1 != start < end:
        
            if form_options and form_options.has_key(str(group.identifier)+option.name):
                value = form_options[str(group.identifier)+option.name][0]
            else:
                value = ''
            
            options_used.append(("mandatory", value))
        
        else:
        
            if form_options and form_options.has_key(str(group.identifier)):
                checked = option.name in form_options[str(group.identifier)]
            else:
                checked = option.default
            
            if option.argument:
                if form_options and form_options.has_key(str(group.identifier)+option.name):
                    value = form_options[str(group.identifier)+option.name][0]
                else:
                    value = ''
                
                # Only include arguments for options if those options
                # are themselves checked.
                
                options_used.append(("checked", (option.name, checked, value)))
            
            else:
                options_used.append(("checked", (option.name, checked, "")))
        
        return options_used
    
    def exclusive_options(self, options):
    
        no_options = 0
        qt_options = 0
        system_options = 0
        
        for option in options:
        
            if option.name.startswith("-no-"):
                no_options += 1
            elif option.name in ("-release", "-opensource", "-shared"):
                return True
            elif option.name.endswith("-endian"):
                return True
            elif option.name.startswith("-qt-"):
                qt_options += 1
            elif option.name.startswith("-system-"):
                system_options += 1
        
        if no_options == 1:
            return True
        elif qt_options == 1 and system_options == 1:
            return True
        else:
            return False
    
    def reconfigure_qt(self, request, form_options):
    
        on_off = {True: 'checked="on" ', False: ""}
        
        header = reconfigure_header_template % {"title": "Configure Qt",
                                    "charset": "utf8"}
        body = []
        options_used = []
        
        for section in self.sections:
        
            body.append(self.tag("h2", section.header))
            body.append(self.tag("p", section.description))
            
            for group in section.groups:
            
                body.append('<div class="group">')
                
                if group.description:
                    body.append(self.tag("p", group.description))
                
                if len(group.options) == 1:
                    control = "checkbox"
                elif self.exclusive_options(group.options):
                    control = "radio"
                else:
                    control = "checkbox"
                
                for option in group.options:
                
                    new_options = self.process_option(group, option, form_options)
                    
                    for option_type, option_value in new_options:
                    
                        if option_type == "mandatory":
                        
                            if option_value:
                                form_value = 'value="' + self.html(option_value) + '" '
                            else:
                                form_value = ''
                            
                            body.append(self.tag("strong", option.name) + ' '
                                        '<input name="' + self.html(str(group.identifier)+option.name) + \
                                        '" ' + form_value + 'type="text"></input>')
                            
                            if option_value:
                                options_used.append(option_value)
                        
                        elif option_type == "checked":
                        
                            name, checked, value = option_value
                            checked = on_off[checked]
                            
                            body.append('<strong><input type="' + control + '" '
                                        'name="' + self.html(str(group.identifier)) + '" '
                                        'value="' + self.html(option.name) + '" ' + \
                                        checked + ">" + \
                                        self.html(option.name) + "</input>"
                                        "</strong>")
                            
                            if option.evaluate:
                                body.append("(evaluated)")
                            
                            if option.argument:
                            
                                if value:
                                    form_value = 'value="' + self.html(value) + '" '
                                else:
                                    form_value = ''
                                
                                body.append('<input name="' + self.html(str(group.identifier)+option.name) + \
                                            '" ' + form_value + 'type="text"></input> ' + self.html(option.argument))
                            
                            if checked and value:
                                options_used.append(name + ' ' + value)
                            elif checked:
                                options_used.append(name)
                    
                    body.append(self.tag("p", option.description))
                
                body.append("</div>")
        
        output_area = []
        
        if options_used:
        
            self.write_output_area(output_area, options_used)
            output_area = [self.tag("p", "Current command line:")] + output_area
        
        footer = reconfigure_footer_template
        
        page = header + "\n".join(output_area) + "\n".join(body) + footer
        
        return ( "text/html", page )
    
    def write_output_area(self, output_area, options_used):
    
        # Write a preformatted text area containing the options used.
       
        self.format_options(output_area, options_used, encode = True)
        
        output_area.insert(0, '<pre class="output">')
        output_area.append("</pre>")
    
    def format_options(self, output_area, options_used, encode = True):
    
        options_used.insert(0, self.script)
        
        cursor = 0
        i = 0
        while i < len(options_used):
        
            if cursor > 0 and cursor + len(options_used[i]) > 72:
            
                output_area[-1] += " \\"
                output_area.append("    ")
                cursor = 0
                continue
            
            if output_area:
                output_area[-1] += " "
            else:
                output_area.append("")
            
            if encode:
                output_area[-1] += self.html(options_used[i])
            else:
                output_area[-1] += options_used[i]
            
            cursor += (1 + len(options_used[i]))
            i += 1
    
    def configure_qt(self, request, form_options):
    
        options_used = []
        
        for section in self.sections:
        
            for group in section.groups:
            
                for option in group.options:
                
                    new_options = self.process_option(group, option, form_options)
                    
                    for option_type, option_value in new_options:
                    
                        if option_type == "mandatory" and option_value:
                        
                            options_used.append(option_value)
                        
                        elif option_type == "checked":
                        
                            name, checked, value = option_value
                            
                            if checked and value:
                                options_used.append(name + ' ' + value)
                            elif checked:
                                options_used.append(name)
        
        options_used.append("-confirm-license")
        
        output_area = []
        self.write_output_area(output_area, options_used[:])
        
        page = processing_template % {
            "charset": "utf8",
            "title": "Qt Configuration Complete",
            "command": "\n".join(output_area),
            "url": "/?action=View+Results"
            }
        
        terminal_output = []
        self.format_options(terminal_output, options_used[:])
        ConfigurationRequestHandler.exit = True
        ConfigurationRequestHandler.output = "\n".join(terminal_output)
        
        return ("text/html", page)
    
    def html(self, text):
    
        return text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
    
    def send_head(self, content_type, data = None):
    
        """send_head(self, content_type)
        \r
        \rCommon code for GET and HEAD commands.
        \r
        \rThis sends the response code and ME headers.
        """
        
        if content_type == None:
        
            #self.send_error(404, "File not found")
            self.send_response(404)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write("")
            return
        
        # Send the ME headers.
        self.send_response(200)
        self.send_header("Content-type", content_type)
        self.end_headers()
        
        if data != None:
        
            # Write the data to the output file.
            self.wfile.write(data)



class ConfigurationServer(SimpleHTTPServer.BaseHTTPServer.HTTPServer):

    def __init__(self, addr, port = 8000):
    
        SimpleHTTPServer.BaseHTTPServer.HTTPServer.__init__(
            self, (addr, port), ConfigurationRequestHandler
            )
        
        self.address = addr
        self.port = port

def indent(text, indent = "  "):

    return "\n".join(map(lambda line: indent + line, text.splitlines()))

def print_sections(sections):

    yes_no = {True: "yes", False: "no"}
    
    for section in sections:
    
        print "Section:", section.header
        print "Description:", section.description
        
        for group in section.groups:
        
            if group.description:
                print " Description:", group.description
        
            for option in group.options:
            
                print "  Option:", option.name
                print "  Argument:", option.argument
                print "  Description:", option.description
                print "  Enabled by default:", yes_no[option.default]
                print "  Evaluated:", yes_no[option.evaluate]


def run_configure(path, options, with_help = False):

    if with_help:
        so, si, se = popen2.popen3(path + " -help " + " ".join(options))
    else:
        so, si, se = popen2.popen3(path + " " + " ".join(options))
    
    si.close()
    
    return so, se


if __name__ == "__main__":

    if len(sys.argv) < 2:
    
        sys.stderr.write("Usage: %s [configure script] [configure options ...]\n" % sys.argv[0])
        sys.exit(1)
    
    options = []
    
    if len(sys.argv) > 2:
        path = sys.argv[1]
        options = sys.argv[2:]
    elif len(sys.argv) == 2:
        path = sys.argv[1]
    else:
        path = os.path.abspath("configure")
    
    so, se = run_configure(path, options, with_help = True)
    
    output = so.read()
    errors = se.read()
    
    if errors:
        sys.stderr.write("The configuration process failed with the following output:\n\n")
        sys.stderr.write(indent(errors))
        sys.stderr.write("\n")
        sys.exit(1)
    
    parser = ConfigurationParser(output)
    sections = parser.parse()
    
    server = ConfigurationServer("")
    ConfigurationRequestHandler.script = path
    ConfigurationRequestHandler.sections = sections
    ConfigurationRequestHandler.exit = False
    ConfigurationRequestHandler.output = "Aborted"
    print "Starting server on http://localhost:%i" % server.port
    
    while not ConfigurationRequestHandler.exit:
        server.handle_request()
    
    if ConfigurationRequestHandler.output != "Aborted":
        print
        print "Copy and paste the following to configure the Qt libraries:"
        print
        print ConfigurationRequestHandler.output
        print
    
    sys.exit()
